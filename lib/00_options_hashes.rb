# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```
def transmogrify(string, options = {})
  defaults = { times: 1,
    upcase: false,
    reverse: false
  }

  updated_options = defaults.merge(options)
  updated_options.each do |option, val|
      if option == :times
      string = string * val
      end
      if option == :upcase
          string = string.upcase if val == true
      end
      if option == :reverse
          string = string.reverse if val == true
      end

  end
  string
end
